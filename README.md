## Usage/Examples

```javascript
import Component from 'LAB-5'

function App() {
  return <Component />
}
```




![Logo](https://i.pinimg.com/564x/ab/a2/d8/aba2d88fa32a05bad7c82ee859c3f6b9.jpg)

## Screenshots

![App Screenshot](https://i.pinimg.com/564x/dc/a4/e8/dca4e800a1217c5b9c1642fb5c793fd4.jpg)

